const fs = require('fs');
const config = require('../default-config');

const fsPromises = fs.promises;
const { COPYFILE_EXCL } = fs.constants;

const jo = require('jpeg-autorotate');

const {takePhoto, createThumb, exitCamera} = require('./external');

const eventActions = {};

const autorotateImage = (inputImg) => {
    const options = {
        quality: 85
    };

    return jo.rotate(inputImg, options)
        .then(({buffer, orientation, dimensions, quality}) => {
            console.log(`Orientation was ${orientation}`);
            console.log(`Dimensions after rotation: ${dimensions.width}x${dimensions.height}`);
            console.log(`Quality: ${quality}`);
            
            return buffer;
        })
};

const saveImage = (buffer, path) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, buffer,(err) => {
            if (err) {
                reject(err);
                return;
            }
            console.log('file has been written');
            resolve();
        });
    });
};

const copyImageFromTemp = (inputPath, outputPath) => {
    return fsPromises.copyFile(inputPath, outputPath, COPYFILE_EXCL)
        .then(() => publishEvent(PHOTO_EVENTS.PHOTO_TAKEN, outputPath));
};

const removeTempImage = (path) => {
    return new Promise((resolve, reject) => {
        fs.unlink(path, (err) => {
            if (err) {
                reject(err);
                return;
            };
            console.log('File deleted!');
            resolve()
        });
    });
};

const publishEvent = (eventType, data) => {
    if (eventActions[eventType]) {
        eventActions[eventType].forEach((action) => action(data));
    }
    return Promise.resolve();
};

const clearEvents = () => {
    Object.getOwnPropertyNames(eventActions).forEach((eventType) => {
        delete eventActions[eventType];
    });
};

const PHOTO_EVENTS = require('../constants/photo-events');

class Camera {
    constructor(options) {
        this.options = Object.assign(config, options || {});
        this.timelapseInProgress = false;
        this.photoInProgress = false;
        this.interval;
    }

    takePhoto(cameraType) {
        const date = new Date().getTime();
        const photoName = `img-${date}.jpg`;
        const thumbName = `img-${date}-thumb.jpg`;
        const tempName = `img-${date}-temp.jpg`;
        const photoPath = `${config.rootPath}/images/${photoName}`;
        const thumbPath = `${config.rootPath}/images/${thumbName}`;
        const tempPath = `${config.rootPath}/images/${tempName}`;
    
        if (!this.photoInProgress) {
            this.photoInProgress = !this.photoInProgress;
            return takePhoto(cameraType, tempPath)
                .catch(console.error)
                .then(() => autorotateImage(tempPath))
                .then((buffer) => saveImage(buffer, photoPath))
                .then(() => copyImageFromTemp(tempPath, photoPath))
                .then(() => removeTempImage(tempPath))
                .then(() => this.options.withThumb ? createThumb(photoPath, thumbPath) : Promise.resolve())
                .catch(console.error)
                .finally(exitCamera)
                .finally(() => this.photoInProgress = !this.photoInProgress);
        }
        return Promise.resolve();
    };

    isTimelapse() {
        return {
            isTimelapse: this.timelapseInProgress,
            interval: this.options.timeInterval
        };
    }

    registerPhotoEvent(eventType, action) {
        if (!eventActions[eventType]) {
            eventActions[eventType] = [];
        }
        eventActions[eventType].push(action);
    }

    start() {
        const takePhoto = () => {
            this.takePhoto(this.options.camera, this.options.withThumb);
        };
        if (this.options.timeInterval < config.timeInterval) {
            this.options.timeInterval = config.timeInterval;
        }

        this.timelapseInProgress = true;

        takePhoto();
        this.interval = setInterval(() => takePhoto(), this.options.timeInterval);
    }

    stop() {
        this.timelapseInProgress = false;
        if (this.interval) {
            clearInterval(this.interval);
        }
        clearEvents();
    }
}

module.exports = Camera;
