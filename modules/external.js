const {exec} = require('child_process');

const commands = {
    takePhoto: (cameraType, path) => {
        return `termux-camera-photo -c ${cameraType} ${path}`;
    },
    createThumb: (image, thumb) => {
        return `ffmpeg -i ${image} -vf scale=-1:66 ${thumb}`
    },
    exitCamera: () => {
        return `pkill com.termux.api`;
    }
};

const execute = (cmd) => {
    console.log(`next command: ${cmd}`);
    return new Promise((resolve, reject) => {
        exec(
            cmd,
            (err) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            }
        );
    });
};

const takePhoto = (cameraType, path) => {
    return execute(commands.takePhoto(cameraType, path));
};

const createThumb = (image, thumb) => {
    return execute(commands.createThumb(image, thumb));
};

const exitCamera = () => {
    return execute(commands.exitCamera());
};

module.exports = {
    takePhoto,
    createThumb,
    exitCamera
}