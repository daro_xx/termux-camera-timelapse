const CAMERAS = require('./constants/cameras');

module.exports = {
  camera: CAMERAS.BACK,
  rootPath: "~/termux-camera-timelapse",
  timeInterval: 10000,
  withThumb: false
};
