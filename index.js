module.exports = {
  Camera: require('./modules/camera'),
  CAMERAS: require('./constants/cameras'),
  PHOTO_EVENTS: require('./constants/photo-events')
}